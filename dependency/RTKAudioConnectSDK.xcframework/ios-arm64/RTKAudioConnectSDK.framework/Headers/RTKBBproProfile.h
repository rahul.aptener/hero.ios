//
//  RTKBBproProfile.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/4/11.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <RTKLEFoundation/RTKLEFoundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * A concrete @c RTKLEProfile subclass dedicated to manage Realtek BBpro-series Bluetooth devices.
 *
 * @discussion This class use @c RTKBBproPeripheral object to represent a discovered remote device. This means that, an @c RTKBBproPeripheral object instead of @c RTKLEPeripheral should be passed to methods defined in superclass that receiving an @c RTKLEPeripheral object parameter, methods return an @c RTKLEPeripheral object actually return an @c RTKBBproPeripherl object  .
 */
DEPRECATED_MSG_ATTRIBUTE("This class is only used for legacy compatibility. Use RTKACConnectionManager class instead")
@interface RTKBBproProfile : RTKLEProfile

@end


NS_ASSUME_NONNULL_END
