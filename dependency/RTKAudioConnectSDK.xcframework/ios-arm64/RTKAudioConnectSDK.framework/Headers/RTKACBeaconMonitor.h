//
//  RTKACBeaconMonitor.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2022/2/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class RTKACBeaconMonitor;

/// Methods an ``RTKACBeaconMonitor`` calls to report that an *Audio Connect* device is detected by its beacon.
@protocol RTKACBeaconMonitorDelegate <NSObject>
@required

/// An *Audio Connect* beacon is detected.
- (void)beaconMonitorDidDetectBBproBeacon:(RTKACBeaconMonitor *)monitor;

@end


/// An object that monitors *Audio Connect* beacons.
@interface RTKACBeaconMonitor : NSObject

/// An object that recevice monitoring events.
@property (weak, nullable) id <RTKACBeaconMonitorDelegate> delegate;


/// Start monitoring *Audio Connect* beacons.
- (void)startMonitoringBBproBeacon;

/// Stop monitoring *Audio Connect* beacons.
- (void)stopMonitoringBBproBeacon;

@end

NS_ASSUME_NONNULL_END
