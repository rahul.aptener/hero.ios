//
//  RTKACRoutineContainer.h
//  RTKLEFoundation
//
//  Created by jerome_gu on 2020/3/31.
//  Copyright © 2020 jerome_gu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RTKLEFoundation/RTKLEFoundation.h>
#import <RTKAudioConnectSDK/RTKACBasicRoutine.h>
#import <RTKAudioConnectSDK/RTKACEQRoutine.h>
#import <RTKAudioConnectSDK/RTKACAudioRoutine.h>
#import <RTKAudioConnectSDK/RTKACMMIRoutine.h>
#import <RTKAudioConnectSDK/RTKACTTSRoutine.h>
#import <RTKAudioConnectSDK/RTKACRHARoutine.h>

NS_ASSUME_NONNULL_BEGIN

/// Common properties and methods for an Audio Connect device connection.
///
/// This class is expected to only be subclass internal in this framwork. Code outside this framework should use concreate subclass directly.
@protocol RTKACRoutineContainer <NSObject>

/// The transport used for exchanging message packets.
@property (readonly) RTKPacketTransport *messageTransport;

/// Return a basic routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have a basic routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACBasicRoutine *basicRoutine;

/// Return a EQ routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have a EQ routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACEQRoutine *EQRoutine;

/// Return an audio routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have an audio routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACAudioRoutine *audioRoutine;

/// Return a MMI routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have a MMI routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACMMIRoutine *MMIRoutine;

/// Return a TTS routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have a TTS routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACTTSRoutine *TTSRoutine;

/// Return a hearing aid routine the conforming class instance has.
///
/// - Returns `nil` if the conforming instance does not have a hearing aid routine.
///
/// Use the returned routine to make interaction with the corresponding device.
@property (nonatomic, readonly, nullable) RTKACRHARoutine *HARoutine;


#pragma mark - State Snapshot

/// Return a data object which is encoded from the state of a conforming object.
- (nullable NSData *)stateSnapshot;

/// Restore the state of a conforming object with the encoded state data.
///
/// - Parameter snapshot: The data returned by ``stateSnapshot``.
- (void)restoreStateWithSnapshot:(NSData *)snapshot;

@end


NS_ASSUME_NONNULL_END
