//
//  RTKBBproError.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/1/23.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSErrorDomain const RTKBBproErrorDomain;

/* RTKBBproErrorDomain Error code */
typedef enum : NSUInteger {
    // MARK: - deprecated error code begin
    RTKBBproErrorCMDDisallow = 0x01,
    RTKBBproErrorCMDUnknown,
    RTKBBproErrorCMDParameterError,
    RTKBBproErrorCMDSOCBusy,
    RTKBBproErrorCMDProcessFail,
    RTKBBproErrorCMDOneWireExtend,
    RTKBBproErrorCMDVersionIncompatible,
    RTKBBproErrorCMDOthers,
    
    RTKBBproErrorParameterInvalid,
    
    RTKBBproErrorSOCNotSupport = 10,
    RTKBBproErrorTTSInstantiation,   /* TTS(Text to Speech)实例化失败 */
    RTKBBproErrorMessageSendFail,
    RTKBBproErrorTTSSynthesizeFail,
    
    RTKBBproErrorEQParameterWaitTimeout,
    
    RTKBBproErrorPreviousNotFinished,
    
    RTKBBproError_prepareFailed,
    RTKBBproError_unkownReason,
    
    RTKBBproErrorDeviceOperateFail,
    RTKBBproError_deviceReportFailure,
    
    // MARK: - deprecated error code end
    
    RTKBBproErrorNotSupport,                        ///< Service is not support by this SDK.
    RTKBBproErrorGATTServiceConformance,                 ///< Device does not have the expected BBpro GATT Service (or attribute is not configured as expected).
    RTKBBproErrorOperationAlreadyStarted,           ///< The same task did already started.
    RTKBBproErrorDeviceNotSupport,                  ///< Opeartion is not supported by device.
    RTKBBproErrorNotARequest,                       ///< Not a realy request send to device.
    
    RTKBBproErrorDeviceRespondsCMDDisallow          =   RTKBBproErrorCMDDisallow,       ///< Device responds with command is not allowed.
    RTKBBproErrorDeviceRespondsCMDUnknown           =   RTKBBproErrorCMDUnknown,        ///< Device responds with command is unknown.
    RTKBBproErrorDeviceRespondsCMDParameterError    =   RTKBBproErrorCMDParameterError, ///< Device responds with command parameter is error.
    RTKBBproErrorDeviceRespondsBusy                 =   RTKBBproErrorCMDSOCBusy,        ///< Device responds with busy.
    RTKBBproErrorDeviceRespondsOtherError           =   RTKBBproErrorCMDOthers,         ///< Device responds with other errors.
    RTKBBproErrorDeviceRespondsOperationFail,       ///< Device responds with a negative result.
    RTKBBproErrorDeviceUpdateVPRingtoneVolumeFail,  ///< Device perform update VP&ringtone volume fail.
    RTKBBproErrorCRCCheckFail,                      ///<The CRC value is incorrect and data verification fails.
    RTKBBproErrorDeviceNotSupportDataCapture,       ///< Device does not support "Data capture scenario"
    RTKBBproErrorDeviceLoadDataCaptureScenarioFail, ///< Device can't load "data capture scenario" because current scenario is not idle
    RTKBBproErrorDeviceExitDataCaptureFail,         ///< Device can’t exit “data capture scenario”.
    
    RTKBBproErrorTransportNotAvailable,             ///< Could not send or receive messages due to no available underlying transport.
} RTKBBproErrorCode;

