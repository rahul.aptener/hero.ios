//
//  RTKACBasicRoutine.h
//  RTKLEFoundation
//
//  Created by jerome_gu on 2020/3/26.
//  Copyright © 2020 jerome_gu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RTKAudioConnectSDK/RTKACRoutine.h>
#import <RTKAudioConnectSDK/RTKBBproType.h>

NS_ASSUME_NONNULL_BEGIN

@class RTKACBasicRoutine;

/// Methods called by an ``RTKACBasicRoutine-c.class-9ibva`` on its delegate to report events.
@protocol RTKBBproBasicStateUpdateReporting <NSObject>
@optional

/// Tells the delegate that remote device did change current used language.
///
/// - Parameter routine: The peripheral that did change the eq index.
/// - Parameter lang: The new language.
- (void)BBproBasicRoutine:(RTKACBasicRoutine *)routine didReceiveLanguageChangeTo:(RTKBBproLanguageType)lang;


/// Tells the delegate that remote device did change current battery.
///
/// - Parameter routine: The peripheral that did change battery level.
/// - Parameter primaryLvl: Battery level of primary bud. `RTKBBproBatteryLevelInvalid` means this value is not valid.
/// - Parameter secondaryLvl: Battery level of secondary bud. `RTKBBproBatteryLevelInvalid` means this value is not valid.
/// - Parameter cradleLvl: Battery charge level of crade if exist. `RTKBBproBatteryLevelInvalid` means this value is not valid.
- (void)BBproBasicRoutine:(RTKACBasicRoutine *)routine
didReceiveUpdateOfPrimaryBatteryLevel:(RTKBBproBatteryLevel)primaryLvl
  secondaryBatteryLevel:(RTKBBproBatteryLevel)secondaryLvl
     cradleBatteryLevel:(RTKBBproBatteryLevel)cradleLvl;


/// Tells the delegate that remote device did change current RWS state.
///
/// - Parameter routine: The peripheral that did update RWS state.
/// - Parameter isOn: Whether RWS is on.
- (void)BBproBasicRoutine:(RTKACBasicRoutine *)routine didReceiveChangeOfRWSState:(BOOL)isOn;


/// Tells the delegate that remote device did change current RWS channel.
///
/// - Parameter routine: The peripheral that did change channel.
/// - Parameter direction: The new direction.
- (void)BBproBasicRoutine:(RTKACBasicRoutine *)routine didReceiveChangeOfRWSChannel:(RTKBBproChannelDirection)direction;

@end


/// An concrete `RTKACRoutine` class that provides functionality to access basic state of an *Audio Connect* device.
@interface RTKACBasicRoutine : RTKACRoutine

/// The delegate object that receives events.
@property (nonatomic, weak) id <RTKBBproBasicStateUpdateReporting> delegate;

/// Return the command version supported by the SDK.
@property (nonatomic, readonly, nullable) NSNumber *SDKCMDVersion;


/// Return the EQ version supported by the SDK.
@property (nonatomic, readonly, nullable) NSNumber *SDKEQVersion;


/// Return the command version info of this device represented.
///
/// The command version value is of unsigned integer type with 2 byte length, which mean calling -[NSNumber unsignedIntValue] to retrieve the value. The Least Significant Byte refer to minor version, and the Most Significant Byte refer to major version. i.e 0x0102 value represent a version of "1.2".
///
/// You use this value to call methods selectively. Some methods is available to some versions.
@property (nonatomic, readonly, nullable) NSNumber *cmdVersionNumber;


/// Return the EQ related command version info of this device represented.
///
/// The command version value is of 16-bit unsigned integer type, which mean calling -[NSNumber unsignedIntValue] to retrieve the value. The Least Significant Byte refer to minor version, and the Most Significant Byte refer to major version. i.e 0x0102 value represent a version of "1.2".
///
/// You use this value to call methods selectively. Some methods is available to some versions.
@property (nonatomic, readonly, nullable) NSNumber *EQVersionNumber;


/// Get the command version and EQ version of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update cmdVersionNumber and EQVersionNumber properties when succeed.
- (void)getVersionInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint16_t cmdVer, uint16_t eqVer))handler;


/// Get the Chip and Package information of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
- (void)getPackageInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproChip chip, NSUInteger packageID))handler;


/// Return a boolean value that indicates if the capability information did determined.
///
/// - Returns `YES` if the information did retrieved from peripheral. return `NO` if the message didn't receive or peripheral does not support report this information.
@property (readonly) BOOL capabilitySettled;


/// Return a boolean value that indicates whether the specified feature is available.
///
/// - Returns `YES` if the specified feature is supported. return `NO` if the message didn't receive or peripheral does not support report this information.
- (BOOL)isAvailableFor:(RTKBBproCapabilityType)capability;


/// Get the feature realted information of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `capabilitySettled` properties when succeed.
- (void)getCapabilityWithCompletionHandler:(nullable RTKLECompletionBlock)handler;


#pragma mark - Product ID

/// Get product information of the connected peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `companyID` and `modelID` when succeed.
- (void)getProductInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproEntityID modelId, RTKBBproEntityID companyId))handler;

#pragma mark - Device Name

/// Get the LE name of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `LEName` property when succeed.
- (void)getLENameWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))handler;


/// Set a new LE name of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `LEName` property when succeed.
- (void)setLEName:(NSString *)name withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Get the BREDR name of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `BREDRName` property when succeed.
- (void)getBREDRNameWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))handler;


/// Set a new BREDR name of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `BREDRName` property when succeed.
- (void)setBREDRName:(NSString *)name withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - Language

/// Get the current using language and supported languages of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `currentLanguage` and `supportedLanguages` properties when succeed.
- (void)getLanguageWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSSet <RTKBBproLanguageType>* supportedLangs, RTKBBproLanguageType currentLang))handler;


/// Set the current language of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `currentLanguage` property when succeed.
- (void)setCurrentLanguage:(RTKBBproLanguageType)lang withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

#pragma mark - Battery

/// Get the current battery level of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `primaryBatteryLevel` and `secondaryBatteryLevel` and `cradleBatteryLevel` properties when succeed.
- (void)getBatteryLevelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproBatteryLevel primary, RTKBBproBatteryLevel secondary, RTKBBproBatteryLevel cradle))handler;


/// Convert primary/secondary battery to left/right battery of this peripheral.
///
/// - Parameter primary: The battery level of primary bud.
/// - Parameter secondary: The battery level of secondary bud.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `leftBatteryLevel`, `rightBatteryLevel` and `singleBatteryLevel` properties when succeed. (RWSState property should be ready before calling this method)
- (void)convertBatteryToLeftRightFromPrimary:(RTKBBproBatteryLevel)primary andSecondary:(RTKBBproBatteryLevel)secondary withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - Volume level

/// Get the VP & Ringtone volume state of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `LVPRingtoneVolumeLevel`, `RVPRingtoneVolumeLevel`, `minLVPRingtoneVolumeLevel`, `maxLVPRingtoneVolumeLevel`, `minRVPRingtoneVolumeLevel` and `maxRVPRingtoneVolumeLevel` properties when succeed.
- (void)getVPRingtoneVolumeStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproVolumeLevel minLevelL, RTKBBproVolumeLevel maxLevelL, RTKBBproVolumeLevel curLevelL, RTKBBproVolumeLevel minLevelR, RTKBBproVolumeLevel maxLevelR, RTKBBproVolumeLevel curLevelR))handler;


/// Set the VP & Ringtone volume level of this peripheral.
///
/// - Parameter levelL: The VP & Ringtone volume of left bud.
/// - Parameter levelR: The VP & Ringtone volume of right bud.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `LVPRingtoneVolumeLevel`, `RVPRingtoneVolumeLevel` properties when succeed.
- (void)setVPRingtoneVolumeLevelOfL:(RTKBBproVolumeLevel)levelL R:(RTKBBproVolumeLevel)levelR withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Set VP & Ringtone volume level and sync info of this peripheral.
///
/// - Parameter levelL: The VP & Ringtone volume of left bud.
/// - Parameter levelR: The VP & Ringtone volume of right bud.
/// - Parameter sync: A boolean value indicates whether left volume and right volume are consistent.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `LVPRingtoneVolumeLevel`, `RVPRingtoneVolumeLevel` and `syncVPRingtoneVolume` properties when succeed.
- (void)setVPRingtoneVolumeLevelOfL:(RTKBBproVolumeLevel)levelL R:(RTKBBproVolumeLevel)levelR Sync:(uint8_t)sync withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Get the global volume level of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `volumeLevel` and `maxVolumeLevel` properties when succeed.
- (void)getVolumeLevelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproVolumeLevel volumeLevel, RTKBBproVolumeLevel maxLevel))handler;


/// Set the global volume level of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `volumeLevel` property when succeed.
- (void)setVolumeLevelTo:(RTKBBproVolumeLevel)level withCompletionHandler:(nullable RTKLECompletionBlock)handler;


#pragma mark - RWS

/// Get the default role information of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `defaultBudRole` property when succeed.
- (void)getDefaultBudRoleWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproBudRole role))handler;


/// Get the bud side information of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `budSide` property when succeed.
- (void)getBudSideWithCompletionHandler:(nullable void(^)(BOOL success, NSError *_Nullable err, RTKBBproBudSide side))handler;


/// Get the default channel information of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `defaultRWSChannelDirection` property when succeed.
- (void)getDefaultRWSChannelDirectionWithCompletionHandler:(nullable void(^)(BOOL success, NSError *_Nullable error, RTKBBproChannelDirection direction))handler;


/// Get the RWS on-off state of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `RWSState` property when succeed.
- (void)getRWSStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))handler;


/// Get the RWS channel flow state of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `RWSChannel` property when succeed.
- (void)getRWSChannelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproChannelDirection channel))handler;


/// Switch the RWS channel state of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `RWSChannel` property when succeed.
- (void)switchRWSChannelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - Legacy profile connection
- (void)connectProfile:(RTKBBproLegacyProfile)profile withCompletionHandler:(nullable RTKLECompletionBlock)handler;

- (void)cancelProfileConnection:(RTKBBproLegacyProfile)profile withCompletionHandler:(nullable RTKLECompletionBlock)handler;


#pragma mark - Multi-link

/// Get the connection count of this peripheral which support multilink.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// A Multi-link supported device may estabilish connection with multiple devices.
- (void)getAppConnectionCountWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))handler;


#pragma mark - BudInfo

/// Get bud info of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `budType`,  `primaryBudSide`, `LRChannel`, `singleOrLeftBattery`, `rightBattery`, `RWSState` and `caseBattery` (if support) when succeed. If the peripheral support this cmd, then call this method to get battery instead of ``RTKACBasicRoutine/getBatteryLevelWithCompletionHandler:``.
-(void)getBudInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

#pragma mark - In ear detection

/// Get the In Ear Detection status of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `inEarDetectionStatus` when succeed.
- (void)getInEarDetectionStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL isOn))handler;


/// Switch the In Ear Detection status of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `inEarDetectionStatus` when succeed.
- (void)switchInEarDetectionStatusWithCompletionHandler:(RTKLECompletionBlock)handler;


/// Inform the device of the application state.
///
/// - Parameter isInForeground: Whether the app is running in the foreground.
/// - Parameter handler: The block to be called once the task completes.
- (void)informOfAppState:(BOOL)isInForeground withCompletionHandler:(nullable RTKLECompletionBlock)handler;


#pragma mark - Find me

/// Get the find me status of remote peripheral.
///
/// Will update `leftBudFindmeEnabled` and `rightBudFindmeEnabled` when succeed.
- (void)getFindmeStatusWithCompletionHandler:(RTKLECompletionBlock)handler;


/// Set find me status on each bud.
///
/// - Parameter isOn: 0x00 disable 0x01enable.
/// - Parameter isRightBud: 0x00 left bud 0x01right bud.
///
/// Will update `leftBudFindmeEnabled` or `rightBudFindmeEnabled` when succeed.
- (void)setFindmeStatus:(BOOL)isOn ofRightBud:(BOOL)isRightBud withCompletionHandler:(RTKLECompletionBlock)handler;


#pragma mark - OTA

/// Inform the charging case to enter ota mode.
///
/// - Parameter enable: 0x00 exit ota mode, 0x01 enter ota mode.
///
/// Will get the BT `address` of charging case when succeed.
- (void)informChargingCaseOTA:(BOOL)enable withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSData *address))handler;

@end


@interface RTKACBasicRoutine (Cache)

/// The company ID of the peripheral
///
/// Affected by ``RTKACBasicRoutine/getProductInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *companyId;


/// The model ID of the peripheral
///
/// Affected by ``RTKACBasicRoutine/getProductInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *modelId;

@property (nonatomic, readonly, nullable) NSNumber *chip;

@property (nonatomic, readonly, nullable) NSNumber *package;


/// Return the cached LE name of device.
///
/// In contrast to `CBPeripheral`.name, this name is retrieved by message exchange through BBpro GATT Service, though they are often same. Affected by ``RTKACBasicRoutine/getLENameWithCompletionHandler:`` and ``RTKACBasicRoutine/setLEName:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSString *LEName;


/// Return the cached BREDR name of this device used for BREDR controller.
///
/// Affected by ``RTKACBasicRoutine/getBREDRNameWithCompletionHandler:`` and ``RTKACBasicRoutine/setBREDRName:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSString *BREDRName;


/// Return the language this peripheral used currently for Voice prompt.
///
/// Affected by ``RTKACBasicRoutine/getLanguageWithCompletionHandler:`` and ``RTKACBasicRoutine/setCurrentLanguage:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) RTKBBproLanguageType currentLanguage;


/// Return a list of languages this peripheral support for Voice prompt.
///
/// Affected by ``RTKACBasicRoutine/getLanguageWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSSet <RTKBBproLanguageType> *supportedLanguages;

/// Return the battery level of the primary role this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by  ``RTKACBasicRoutine/getBatteryLevelWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *primaryBatteryLevel;

/// Return the battery level of the secondary role this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by ``RTKACBasicRoutine/getBatteryLevelWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *secondaryBatteryLevel;

/// Return the battery level of the cradle this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by ``RTKACBasicRoutine/getBatteryLevelWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *cradleBatteryLevel;


/// Return the battery level of the left role this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by ``RTKACBasicRoutine/convertBatteryToLeftRightFromPrimary:andSecondary:withCompletionHandler``.
@property (nonatomic, readonly, nullable) NSNumber *leftBatteryLevel;


/// Return the battery level of the right role this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by ``RTKACBasicRoutine/convertBatteryToLeftRightFromPrimary:andSecondary:withCompletionHandler``.
@property (nonatomic, readonly, nullable) NSNumber *rightBatteryLevel;


/// Return the battery level of the single bud this device last cached.
///
/// Scalar value is of `RTKBBproBatteryLevelInvalid` type. Affected by ``RTKACBasicRoutine/convertBatteryToLeftRightFromPrimary:andSecondary:withCompletionHandler``.
@property (nonatomic, readonly, nullable) NSNumber *singleBatteryLevel;


/// Return the VP Ringtone volume level of the left bud this device last cached.
///
/// Scalar value is of `RTKBBproVolumeLevel` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``, ``RTKACBasicRoutine/setVPRingtoneVolumeLevelOfL:R:withCompletionHandler:`` and ``RTKACBasicRoutine/setVPRingtoneVolumeLevelOfL:R:Sync:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *LVPRingtoneVolumeLevel;


/// Return the VP Ringtone volume level of the right bud this device last cached.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``, ``RTKACBasicRoutine/ setVPRingtoneVolumeLevelOfL:withCompletionHandler:`` and ``RTKACBasicRoutine/setVPRingtoneVolumeLevelOfL:R:Sync:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *RVPRingtoneVolumeLevel;


/// Return the VP Ringtone minimum volume level of the left bud this device last cached.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *minLVPRingtoneVolumeLevel;


/// Return the VP Ringtone maximum volume level of the left bud this device last cached.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *maxLVPRingtoneVolumeLevel;


/// Return the VP Ringtone minimum volume level of the right bud this device last cached.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *minRVPRingtoneVolumeLevel;


/// Return the VP Ringtone maximum volume level of the right bud this device last cached.
///
/// Scalar value is of `RTKBBproVolumeLevel` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *maxRVPRingtoneVolumeLevel;


/// Return whether the left and right VP Ringtone level are in sync.
///
/// Scalar value is of `BOOL` type. Affected by ``RTKACBasicRoutine/getVPRingtoneVolumeStateWithCompletionHandler:`` and ``RTKACBasicRoutine/setVPRingtoneVolumeLevelOfL:R:Sync:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *syncVPRingtoneVolume;


/// Return the current global volume level.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVolumeLevelWithCompletionHandler:`` and ``RTKACBasicRoutine/setVolumeLevelTo:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *volumeLevel;


/// Return the maximum volume level.
///
/// Scalar value is of ``RTKBBproVolumeLevel`` type. Affected by ``RTKACBasicRoutine/getVolumeLevelWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *maxVolumeLevel;


/// Return the RWS on-off state of the this device last cached.
///
/// Scalar value is of `BOOL` type. Affected by ``RTKACBasicRoutine/getRWSStateWithCompletionHandler:`` and device active notification.
@property (nonatomic, nullable, readonly) NSNumber *RWSState;


/// Return the RWS channel state of the this device last cached.
///
/// Scalar value is of ``RTKBBproChannelDirection`` type. Affected by ``RTKACBasicRoutine/getRWSChannelWithCompletionHandler:`` and device active notification.
@property (nonatomic, readonly, nullable) NSNumber *RWSChannel;


/// Return the default role last cached of this device.
///
/// Scalar value is of ``RTKBBproBudRole`` type. Affected by ``RTKACBasicRoutine/getDefaultBudRoleWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *defaultBudRole;


///Return the bud side last cached of this device.
///
/// Scalar value is of ``RTKBBproBudSide`` type. Affected by ``RTKACBasicRoutine/getBudSideWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *budSide;


/// Return the default channel last cached of this device.
///
/// Scalar value is of ``RTKBBproChannelDirection`` type. Affected by ``RTKACBasicRoutine/getRWSDefaultChannelSideWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *defaultRWSChannelDirection;


/// Return the budType of this peripheral last cached. 0x00 Single, 0x01 RWS
///
/// Scalar value is of ``RTKBudType`` type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *budType;


/// Return the connected bud of this peripheral last cached.
///
/// Scalar value is of ``RTKBBproBudSide`` type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *primaryBudSide;


/// Return the channel corresponding to left and right of this peripheral last cached.
///
/// Scalar value is of ``RTKBBproLRChannel`` type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *LRChannel;


/// Return the left battery of this peripheral last cached.
///
/// Scalar value is of unsigned integer type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *singleOrLeftBattery;


/// Return the right battery of this peripheral last cached.
///
/// Scalar value is of unsigned integer type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *rightBattery;


/// Return the case battery of this peripheral last cached.
///
/// Scalar value is of unsigned integer type. Affected by ``RTKACBasicRoutine/getBudInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *caseBattery;


/// Returns a value indicating if In Ear Dectection is on.
///
/// Scalar value is of `BOOL` type. Affected by ``RTKACBasicRoutine/getInEarDetectionStatusWithCompletionHandler:`` and ``RTKACBasicRoutine/switchInEarDetectionStatusWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *inEarDetectionStatus;


/// Returns a value indicating whether the find me status of left bud is enabled.
///
/// Scalar value is of `BOOL` type. Affected by ``RTKACBasicRoutine/getFindmeStatusWithCompletionHandler:`` and ``RTKACBasicRoutine/setFindmeStatus:ofRightBud:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *leftBudFindmeEnabled;


/// Returns a value indicating whether the find me status of right bud is enabled.
///
/// Scalar value is of `BOOL` type. Affected by ``RTKACBasicRoutine/getFindmeStatusWithCompletionHandler:`` and ``RTKACBasicRoutine/setFindmeStatus:ofRightBud:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *rightBudFindmeEnabled;


@end


NS_ASSUME_NONNULL_END
