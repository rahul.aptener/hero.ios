//
//  RTKACRHARoutine.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2022/7/14.
//

#import <RTKAudioConnectSDK/RTKACRoutine.h>
#import <RTKAudioConnectSDK/RTKBBproType.h>
#import <RTKAudioConnectSDK/RTKHATypes.h>
#import <RTKAudioConnectSDK/RTKACBasicRoutine.h>

NS_ASSUME_NONNULL_BEGIN

/// An object you use to perform communication related to Hearing Aid feature with connected devices.
@interface RTKACRHARoutine : RTKACRoutine

/// The basic routine this Audio routine depends.
@property RTKACBasicRoutine *basicRoutine;

/// Get HA program count of the connected device.
- (void)getProgramCountWithCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSUInteger count))handler;

/// Get the program index used currently.
- (void)getCurrentProgramWithCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSUInteger idx))handler;

/// Switch to use a program identified with index.
- (void)setCurrentProgram:(NSUInteger)programIdx withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request to reset the program settings currently used in the connected device.
- (void)resetCurrentProgramWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Get all program names.
- (void)getAllProgramNamesWithCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSArray <NSString*> *names))handler;

/// Get program name of a specified program
- (void)getNameOfProgram:(NSUInteger)programIndex withCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSString *name))handler;

/// Set a new name of a specified program.
- (void)setName:(NSString *)name ofProgram:(NSUInteger)programIdx withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Load HA configuration information of the connected device.
- (void)loadConfigurationWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Get the APT Volume state of the connected device.
- (void)getAPTVolumeWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, HAVolumeLevel LVolume, HAVolumeLevel RVolume, HABalanceType balance))handler;

- (void)getAPTVolumeAdjustmentSynchronizationStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL ajustSynchronously))handler;

- (void)setAPTVolumeAdjustmentSynchronization:(BOOL)synchronously withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Set the HA volume and balance of the connnected device.
- (void)setConfigurationWithBalance:(HABalanceType)balance
                    volumeOfLeftBud:(HAVolumeLevel)LVolume
                   volumeOfRightBud:(HAVolumeLevel)RVolume
                  completionHandler:(nullable RTKLECompletionBlock)handler;

/// Set the HA configuration of the connnected device.
- (void)setConfigurationWithGainLevelsOfLeftBud:(HAGainLevel*)lGainLevels
                           gainLevelsOfRightBud:(HAGainLevel*)rGainLevels
                              completionHandler:(nullable RTKLECompletionBlock)handler;

/// Get the APT Volume Mute state of the connected device.
- (void)getAPTVolumeMuteStatusWithCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, RTKHAAPTVolumeMuteState leftBud, RTKHAAPTVolumeMuteState rightBud))handler;

/// Set the APT Volume Mute state of the connected device.
- (void)setAPTVolumeMute:(BOOL)isMuted ofBud:(RTKBBproBudSide)bud withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set NR (Noice Reduction) state of the connected device.
- (void)setNoiceReductionEnable:(BOOL)enabled
                           mode:(RTKHANRMode)mode
                  aggresiveness:(HAGainLevel)level
          withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set FBC state of the connected device.
- (void)setFBCEnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set OVP state of the connected device.
- (void)setOVPEnable:(BOOL)enabled aggresiveness:(HAGainLevel)level withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set Beamforming state of the connected device.
- (void)setBeamformingEnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set the Wind Noice Reduction enable state of the connected device.
- (void)setWNREnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set the Impulse Noice Reduction enable state of the connected device.
- (void)setINREnable:(BOOL)enabled
           intensity:(NSUInteger)intensity
         sensitivity:(NSUInteger)sensitivity
withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set the Real Natural Sound enable state of the connected device.
- (void)setRNSEnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Set if HA algorithm should be used.
- (void)setHABypassed:(BOOL)yesOrNo withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Get a list of dBSPL references of the specified ear bud.
///
/// Once request is completed successfully, the freqReferences dictionary has keys that represents frequency integer and values that represents dbspl value.
- (void)getAudioOutputDBSPLReferencesOfBud:(RTKBBproBudSide)bud withCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSDictionary <NSNumber*, NSNumber*> *_Nullable freqReferences))handler;

/// Get the measured dbFS about the recent outputed audio.
- (void)getAudioOutputDBFSOfBud:(RTKBBproBudSide)bud withCompletionHandler:(nullable void (^)(BOOL success, NSError * _Nullable error, NSInteger db))handler;

/// Apply compensation to HA based on the hearing loss information
///
/// - Parameter thresholds: A list of hearing loss of one of the user's ear at different frequency.
/// - Parameter handler: A block to be invoked when this task completes.
- (void)applyCompensationForHearingLoss:(NSSet <RTKACHAPitchThreshold*>*)thresholds
                      completionHandler:(nullable RTKLECompletionBlock)handler;

/// Remove compensation to HA based on the hearing loss information
///
/// - Parameter thresholds: A list of hearing loss of one of the user's ear at different frequency.
/// - Parameter handler: A block to be invoked when this task completes.
- (void)removeCompensationForHearingLoss:(NSSet <RTKACHAPitchThreshold*>*)thresholds
                       completionHandler:(nullable RTKLECompletionBlock)handler;

/// Whether the compensation regarding to Hearing loss is applied.
@property (readonly, getter=isCompensatedForHearingLoss) BOOL compensatedForHearingLoss;

/// Update the output DRC parameter of the connected device.
///
/// - Parameter state: A structure containing updated state information.
/// - Parameter handler: The handler to be called once this task completes.
- (void)setOutputDRC:(RTKHADRCState)state withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request device to create a player instance for playing pure tone.
///
/// - Parameter bud: A specific ear bud.
/// - Parameter handler: A completion handler block to be invoked once task completes.
- (void)createPureTonePlayerIn:(RTKHAApplyBud)bud withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request device to destroy a created player instance for playing pure tone.
///
/// - Parameter bud: A specific ear bud.
/// - Parameter handler: A completion handler block to be invoked once task completes.
- (void)destroyPureTonePlayerIn:(RTKHAApplyBud)bud withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request device to start playing pure tone.
///
/// - Parameter bud: A specific ear bud.
/// - Parameter handler: A completion handler block to be invoked once task completes.
- (void)startPureTonePlayerIn:(RTKHAApplyBud)bud withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request device to stop playing pure tone.
///
/// - Parameter bud: A specific ear bud.
/// - Parameter handler: A completion handler block to be invoked once task completes.
- (void)stopPureTonePlayerIn:(RTKHAApplyBud)bud withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/// Request device to update the pure tone parameters.
///
/// - Parameters:
///     - freq: The frequency of the pure tone.
///     - db: The intensity in dBFS of the pure tone.
///     - bud: A specific ear bud.
///     - duration: The interval for playing the pure tone.
///     - handler: A completion handler block to be invoked once task completes.
- (void)updatePureToneFrequency:(NSUInteger)freq intensity:(NSInteger)db ofPureTonePlayerIn:(RTKHAApplyBud)bud for:(NSTimeInterval)duration withCompletionHandler:(nullable RTKLECompletionBlock)handler;

@end


@interface RTKACRHARoutine (Cache)

/// Returns the number object that indicates solved program count.
@property (nullable, readonly) NSNumber *programCount;

/// Returns the number object that indicates solved program names.
@property (nullable, readonly) NSArray <NSString *> *programNames;

/// Returns the number object that indicates solved program index current used .
@property (nullable, readonly) NSNumber *programIndex;

/// Returns the number object that indicates solved name of current used program.
@property (nullable, readonly) NSString *programName;

/// Return a number that indicates if the left ear bud APT is muted.
@property (nullable, readonly) NSNumber *leftBudAPTMuted;

/// Return a number that indicates if the right ear bud APT is muted.
@property (nullable, readonly) NSNumber *rightBudAPTMuted;

/// Returns the number object that indicates solved APT balance.
@property (nullable, readonly) NSNumber *balance;

@property (readonly) RTKHAGainConfiguration configuration;

/// Returns the number object that indicates solved APT volume of left bud.
@property (nullable, readonly) NSNumber *leftBudAPTVolume;

/// Returns the number object that indicates solved APT volume of right bud.
@property (nullable, readonly) NSNumber *rightBudAPTVolume;

/// Returns the number object that indicates whether APT volume adjustment of left bud and right bud occurs synchronously.
@property (nullable, readonly) NSNumber *APTVolumeAdjustmentSynchronously;

/// Returns the number object that indicates solved APT gain levels of left bud.
@property (nullable, readonly) NSArray <NSNumber*> *leftAPTGainLevels;

/// Returns the number object that indicates solved APT gain levels of right bud.
@property (nullable, readonly) NSArray <NSNumber*> *rightAPTGainLevels;

/// Returns the number object that indicates solved NR enable status.
@property (nullable, readonly) NSNumber *NREnabled;

/// Returns the number object that indicates solved NR level status.
@property (nullable, readonly) NSNumber *NRLevel;

/// Returns the number object that indicates solved NR mode status.
@property (nullable, readonly) NSNumber *NRMode;

/// Returns the number object that indicates solved FBC enable status.
@property (nullable, readonly) NSNumber *FBCEnabled;

/// Returns the number object that indicates solved OVP enable status.
@property (nullable, readonly) NSNumber *OVPEnabled;

/// Returns the number object that indicates solved OVP level status.
@property (nullable, readonly) NSNumber *OVPLevel;

/// Returns the number object that indicates solved Beamforming enable status.
@property (nullable, readonly) NSNumber *beamformingEnabled;

/// Returns the number object that indicates solved dehowling enable status.
@property (nullable, readonly) NSNumber *dehowlingEnabled;

/// Returns the number object that indicates solved dehowling level status.
@property (nullable, readonly) NSNumber *dehowlingLevel;

/// Returns the number object that indicates solved WNR enable status.
@property (nullable, readonly) NSNumber *WNREnabled;

/// Returns the number object that indicates solved INR enable status.
@property (nullable, readonly) NSNumber *INREnabled;

/// Returns the number object that indicates solved INR intensity status.
@property (nullable, readonly) NSNumber *INRIntensity;

/// Returns the number object that indicates solved INR sensitivity status.
@property (nullable, readonly) NSNumber *INRSensitivity;

/// Returns a number object that indicates solved WNR enable status.
@property (nullable, readonly) NSNumber *RNSEnabled;

/// Returns a NSNumber that indicates if HA Algorithm is bypassed.
@property (nullable, readonly) NSNumber *HABypassed;

/// Returns a `NSValue` object embeding an `RTKHADRCState` value.
@property (nullable, readonly) NSValue *outputDRCState;

@end

NS_ASSUME_NONNULL_END
