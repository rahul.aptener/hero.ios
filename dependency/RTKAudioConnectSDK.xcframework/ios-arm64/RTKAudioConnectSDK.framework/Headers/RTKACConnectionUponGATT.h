//
//  RTKACConnectionUponGATT.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/10/18.
//

#import <RTKLEFoundation/RTKLEFoundation.h>
#import <RTKAudioConnectSDK/RTKACRoutineContainer.h>

NS_ASSUME_NONNULL_BEGIN

/// A GATT connection with an Audio Connect feature implemented device.
///
/// An ``RTKACConnectionManager`` creates an instance of this class if it discovers a connection with *Audio Connect* device using GATT.
@interface RTKACConnectionUponGATT : RTKConnectionUponGATT <RTKACRoutineContainer>

@end

NS_ASSUME_NONNULL_END
