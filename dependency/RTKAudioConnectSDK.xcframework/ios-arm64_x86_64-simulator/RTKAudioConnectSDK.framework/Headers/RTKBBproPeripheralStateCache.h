//
//  RTKBBproPeripheralStateCache.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/9/17.
//

#import <Foundation/Foundation.h>
#import <RTKAudioConnectSDK/RTKBBproType.h>
#import <RTKAudioConnectSDK/RTKBBproEQSetting.h>
#import <RTKAudioConnectSDK/RTKBBproEQSettingPlaceholder.h>


NS_ASSUME_NONNULL_BEGIN


/**
 * An object which store state value of a remote device temporarily.
 *
 * @discussion A @c RTKBBproPeripheralStateCache defines properties for use to retrieve saved state value. If a state is not cached, the property return a nil. A state is cached if any activity happened which can determine a state, such as a message received from device. For example, a successful @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] call will cache batterylevel value in  primaryBatteryLevel property.
 *
 * All most all properties are object type and nullable, a nil property value means the state not determined (not cached) yet. Most properties use @c NSNumber objects to wrap a scaler value, refer to property document for the type when extract value.
 *
 * All most every properties is KVO-applicable, which means that you can get notified when the cached state changed by observing it.
 *
 * You call @c RTKBBproPeripheral.state to retrieve a instance associated with remote device instead of create one by yourself.
 */
@interface RTKBBproPeripheralStateCache : NSObject

/**
 * Clear all saved value.
 */
- (void)clear;

/**
 * The company ID of the peripheral
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getProductInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *companyId;

/**
 * The model ID of the peripheral
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getProductInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *modelId;

/**
 * Return the cached name of device.
 *
 * @discussion In contrast to @c CBPeripheral.name, this name is retrieved by message exchange through BBpro GATT Service, though they are often same. Affected by @c -[RTKBBproPeripheral @c getLENameWithCompletion:] and @c -[RTKBBproPeripheral @c setLEName:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSString *LEName;

/// Return the cached name of this device used for BREDR controller.
/// Affected by @c -[RTKBBproPeripheral @c getBREDRNameWithCompletion:] and @c -[RTKBBproPeripheral @c setBREDRName:withCompletion:] .
@property (nonatomic, readonly, nullable) NSString *BREDRName;


/**
 * Return the language this peripheral used currently for Voice prompt.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getLanguageWithCompletion:] and @c -[RTKBBproPeripheral @c setCurrentLanguage:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) RTKBBproLanguageType currentLanguage;

/**
 * Return a list of languages this peripheral support for used for Voice prompt.
 * @discussion Affected by @c -[RTKBBproPeripheral @c getLanguageWithCompletion:].
 */
@property (nonatomic, readonly, nullable) NSSet <RTKBBproLanguageType> *supportedLanguages;



/**
 * Return the battery level of the primary role device last cached.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletion:] and @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *primaryBatteryLevel;

/**
 * Return the battery level of the secondary role bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletion:] and @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *secondaryBatteryLevel;

/**
 * Return the battery level of the cradle last cached if the device contain cradle.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *cradleBatteryLevel;


/**
 * Return the battery level of the left role device last cached.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletion:] and @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *leftBatteryLevel;

/**
 * Return the battery level of the right role bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletion:] and @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightBatteryLevel;

/**
 * Return the battery level of the single bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproBatteryLevelInvalid type. Affected by @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletion:] and @c -[RTKBBproPeripheral @c getBatteryLevelWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *singleBatteryLevel;


/**
 * Return the RWS on-off state of the this device last cached.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getRWSStateWithCompletion:] and device active notification.
 */
@property (nonatomic, nullable, readonly) NSNumber *RWSState;

/**
 * Return the RWS channel state of the this device last cached.
 *
 * @discussion Scalar value is of @c RTKBBproChannelDirection type. Affected by @c -[RTKBBproPeripheral @c getRWSChannelWithCompletion:] and device active notification.
 */
@property (nonatomic, readonly, nullable) NSNumber *RWSChannel;


/**
 * Return the default role last cached of this device.
 *
 * @discussion Scalar value is of @c RTKBBproBudRole type. Affected by @c -[RTKBBproPeripheral @c getDefaultBudRoleWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *defaultBudRole;


/**
 * Return the side last cached of this device.
 *
 * @discussion Scalar value is of @c RTKBBproBudSide type. Affected by @c -[RTKBBproPeripheral @c getBudSideWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *budSide;


/**
 * Return the default channel last cached of this device.
 *
 * @discussion Scalar value is of @c RTKBBproChannelDirection type. Affected by @c -[RTKBBproPeripheral @c getRWSDefaultChannelSideWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *defaultChannelSide;


/**
 * Return the listening mode switch cycle last cached of this device.
 *
 * @discussion Scalar value is of @c RTKBBproListeningModeSwitchCycle type. Affected by @c -[RTKBBproPeripheral @c getListeningModeSwitchCycleStateWithCompletion:] and @c -[RTKBBproPeripheral @c setListeningSwitchCycle:withCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSNumber *listeningSwitchCycle;

#pragma mark - Audio Pass Through

/**
 * Return the APT on-off state of the this device last cached.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getAPTStateWithCompletion:] and @c -[RTKBBproPeripheral @c switchAPTStateWithCompletion:] and device active notification.
 */
@property (nonatomic, nullable, readonly) NSNumber *APTState;


/**
 * Return the LLAPT on-off state of the this device last cached.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getLLAPTStateWithCompletion:] and @c -[RTKBBproPeripheral @c setLLAPTEnable:withCompletionHandler:] and device active notification.
 */
@property (nonatomic, nullable, readonly) NSNumber *LLAPTEnabled;


#pragma mark - ANC mode

/**
 * Return the ANC on-off state last cached of this device.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getANCStateWithCompletion:] and @c -[RTKBBproPeripheral @c setANCEnable:withCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSNumber *ANCEnabled;


/**
 * Return the ANC scenario current used last cached of this device.
 *
 * @discussion Scalar value is of @c RTKBBproANCScenario type. Affected by @c -[RTKBBproPeripheral @c getANCStateWithCompletion:] and @c -[RTKBBproPeripheral @c setCurrentANCModeIndex:withCompletionHandler:].
 */
@property (nonatomic, nullable) NSNumber *currentANCModeIndex;

/**
 * Return a list of ANC modes last cached supported by this device.
 *
 * @discussion For each item, scalar value is of @c RTKBBproANCScenario type. Affected by @c -[RTKBBproPeripheral @c getANCStateWithCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber*> *ANCModes;

/**
 * Return a list of @c RTKBBproEQSetting objects which representing EQ settings in remote device runing ANC mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQEntryCountOfMode:withCompletionHandler:] with @c RTKBBproEQMode_anc . The returned @c RTKBBproEQSetting object may not be fully determine the parameter. Call @c -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *ANCModeEQSettings;

/**
 * Return an @c RTKBBproEQSetting instance which currently used for ANC mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getCurrentEQIndexOfMode:withCompletionHandler:] and @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:]  with @c RTKBBproEQMode_anc .
 */
@property (readonly, nullable) RTKBBproEQSetting *ANCModeEQSettingInUse;

#pragma mark - ANC Scenario Group Setting

/**
 * Return the count of ANC groups last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getANCScenarioChooseInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *ANCGroupsNumber;

/**
 * Return a list of ANC scenarios last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getANCScenarioChooseInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *ANCScenarios;

#pragma mark - EQ
/**
 * Return cached current EQ index used for a legacy SOC implementation.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getLegacyCurrentEQIndexStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setLegacyCurrentEQIndexTo:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *legacyCurrentEQIndex;

/**
 * Return cached supported EQ indexes used for a legacy SOC implementation.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getLegacyCurrentEQIndexStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *legacySupportedEQIndexes;

/**
 * Return an @c RTKBBproEQSetting instance which currently used of a legacy implementation.
 */
@property (readonly, nullable) RTKBBproEQSetting *legacyEQSettingInUse;


/**
 * Return a list of @c RTKBBproEQSetting objects which representing EQ settings in remote device runing normal mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQEntryCountOfMode:withCompletionHandler:]  with @c RTKBBproEQMode_normal . The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *normalModeEQSettings;

/**
 * Return an @c RTKBBproEQSetting instance which currently used for normal mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getCurrentEQIndexOfMode:withCompletionHandler:] and @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:]  with @c RTKBBproEQMode_normal .
 */
@property (readonly, nullable) RTKBBproEQSetting *normalModeEQSettingInUse;


#pragma mark - VP & Ringtone volume

/**
 * Return the VP Ringtone volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setVPRingtoneVolumeLevelOfL:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *LVPRingtoneVolumeLevel;

/**
 * Return the VP Ringtone volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setVPRingtoneVolumeLevelOfL:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *RVPRingtoneVolumeLevel;

/**
 * Return the VP Ringtone minimum volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *minLVPRingtoneVolumeLevel;

/**
 * Return the VP Ringtone maximum volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxLVPRingtoneVolumeLevel;

/**
 * Return the VP Ringtone minimum volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *minRVPRingtoneVolumeLevel;

/**
 * Return the VP Ringtone maximum volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of @c RTKBBproVolumeLevel type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxRVPRingtoneVolumeLevel;

/**
 * Return whether the left and right level are in sync.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getVPRingtoneVolumeStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setVPRingtoneVolumeLevelOfL:R:Sync:withCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSNumber *syncVPRingtoneVolume;

#pragma mark - MMI

/**
 * Return a list of MMI actions supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c RTKBBproPeripheralMMI type. Affected by @c -[RTKBBproPeripheral @c getSupportedMMIsWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedMMIs;

/**
 * Return a list of MMI motions supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c RTKBBproPeripheralMMIClickType type.  Affected by @c -[RTKBBproPeripheral @c getSupportedClicksWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedClicks;

/**
 * Return a list of call status for MMI supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c RTKBBproPeripheralMMIStatus type.  Affected by @c -[RTKBBproPeripheral @c getSupportedPhoneStatusWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedPhoneStatus;

/**
 * Return a list of MMI mapping by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c RTKBBproMMIMapping type.  Affected by @c -[RTKBBproPeripheral @c getMMIKeyMappingWithCompletionHandler:]  @c -[RTKBBproPeripheral @c setMMIKeyMapping:withCompletionHandler:] and @c -[RTKBBproPeripheral @c resetMMIKeyMappingWithCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSArray <NSValue*> *MMIKeyMappings;

/**
 * Return a list of MMI mapping by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c RTKBBproMMIMapping type.  Affected by @c -[RTKBBproPeripheral @c getRWSKeyMMIMapWithCompletionHandler:]  @c -[RTKBBproPeripheral @c setRWSKeyMMIMap:withCompletionHandler:] and @c -[RTKBBproPeripheral @c resetRWSKeyMMIMapWithCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSArray <NSValue*> *RWSMMIKeyMappings;

/**
 * Return whether the button locked is on of this peripheral last cached.
 *
 * @discussion For each item, scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getMMIButtonLockStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c switchMMIButtonLockStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *buttonLocked;



#pragma mark - Gaming mode
/**
 * Return whether the gaming mode is on of this peripheral last cached.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getLowLatencyStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c switchGamingModeEnableWithCompletionHandler:] .
 */
@property (readonly, nullable) NSNumber *gamingModeEnabled;

/**
 * Return the low lantency value of this peripheral last cached.
 *
 * @discussion Scalar value is of @c unsigned @c integer type. Affected by @c -[RTKBBproPeripheral @c getLowLatencyStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setLowLatencyLevel:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *lowLatencyValue;

/**
 * Return the low lantency level of this peripheral last cached.
 *
 * @discussion Scalar value is of @c unsigned @c integer type. Affected by @c -[RTKBBproPeripheral @c getMMIButtonLockStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c switchMMIButtonLockStateWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *lowLatencyLevel;

/**
 * Return the maximum low lantency level of this peripheral last cached.
 *
 * @discussion Scalar value is of @c unsigned @c integer type. Affected by @c -[RTKBBproPeripheral @c getLowLatencyStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setLowLatencyLevel:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxLowLatencyLevel;


/**
 * Return a list of @c RTKBBproEQSetting objects which representing EQ settings in remote device excuting gaming mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQEntryCountOfMode:withCompletionHandler:]  with @c RTKBBproEQMode_gaming . The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *gamingModeEQSettings;

/**
 * Return the current @c RTKBBproEQSetting used of the remote device in gaming mode.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getCurrentEQIndexOfMode:withCompletionHandler:] and @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:]  with @c RTKBBproEQMode_gaming .
 */
@property (readonly, nullable) RTKBBproEQSetting *gamingModeEQSettingInUse;



#pragma mark - LLAPT

/**
 * Return the LLAPT scenario current used last cached of this device.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getLLAPTStateWithCompletion:], @c -[RTKBBproPeripheral @c setCurrentLLAPTScenarioIndex:withCompletionHandler:] and @c -[RTKBBproPeripheral @c setEnable:ofLLAPTScenarioIndex:withCompletionHandler:] .
 */
@property (nonatomic, nullable) NSNumber *currentLLAPTScenarioIndex;

/**
 * Return a list of LLAPT scenarios last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getLLAPTStateWithCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber*> *LLAPTScenarios;

#pragma mark - LLAPT Scenario Group Setting

/**
 * Return the count of LLAPT groups last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getLLAPTScenarioChooseInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *LLAPTGroupsNumber;

/**
 * Return a list of LLAPT scenarios last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getLLAPTScenarioChooseInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *scenarios;

#pragma mark - APT NR
/**
 * Return whether the APT NR is on of this peripheral last cached.
 *
 * @discussion Scalar value is of BOOL type. Affected by @c -[RTKBBproPeripheral @c getAPTNRStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setAPTNRState:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *APTNRState;

#pragma mark - APT EQ
/**
 * Return the count of APT EQ entry of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTEQSettingsWithCompletionHandler:] and @c -[RTKBBproPeripheral @c getAPTModeEQEntryCountWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *APTEQCount;

/**
 * Return the index of APT EQ entry current used of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getCurrentAPTEQIndexWithCompletion:] and @c -[RTKBBproPeripheral @c setCurrentAPTEQIndex:completion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *APTEQIndex;

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in Left side bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettingsOfLeft;

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in Right side bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettingsOfRight;

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in both bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettings;


#pragma mark - APT Volume And Brightness

/**
 * Return whether the left and right volume are in sync.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeSyncStateWithCompletionHandler:] and @c -[RTKBBproPeripheral @c switchAPTVolumeSyncStateWithCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSNumber *syncAPTVolume;

/**
 * Return the maximum Main APT volume level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxMainAPTVolumeLevel;

/**
 * Return the maximum Sub APT volume level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxSubAPTVolumeLevel;

/**
 * Return the current Main APT volume level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTVolumeLevelofType:withLeftVolume:andRightVolume:withCompletion:] and @c -[RTKBBproPeripheral @c getAPTVolumeStatusWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *leftMainAPTVolumeLevel;

/**
 * Return the current Sub APT volume level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:] , @c -[RTKBBproPeripheral @c setAPTVolumeLevelofType:withLeftVolume:andRightVolume:withCompletion:] and @c -[RTKBBproPeripheral @c getAPTVolumeStatusWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *leftSubAPTVolumeLevel;

/**
 * Return the current Main APT volume level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:] , @c -[RTKBBproPeripheral @c setAPTVolumeLevelofType:withLeftVolume:andRightVolume:withCompletion:] and @c -[RTKBBproPeripheral @c getAPTVolumeStatusWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightMainAPTVolumeLevel;

/**
 * Return the current Sub APT volume level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeInfoWithCompletionHandler:] , @c -[RTKBBproPeripheral @c setAPTVolumeLevelofType:withLeftVolume:andRightVolume:withCompletion:] and @c -[RTKBBproPeripheral @c getAPTVolumeStatusWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightSubAPTVolumeLevel;

/**
 * Return the maximum Main APT Brightness level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxMainBrightnessLevel;

/**
 * Return the maximum Sub APT Brightness level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *maxSubBrightnessLevel;

/**
 * Return the current Main APT Brightness level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:withCompletion:],@c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:Sync:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *leftMainBrightnessLevel;

/**
 * Return the current Sub APT Brightness level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:withCompletion:],@c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:Sync:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *leftSubBrightnessLevel;

/**
 * Return the current Main APT Brightness level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:withCompletion:],@c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:Sync:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightMainBrightnessLevel;

/**
 * Return the current Sub APT Brightness level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:withCompletion:],@c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:] and @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:Sync:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightSubBrightnessLevel;

/**
 * Return whether the left and right brightness are in sync.
 *
 * @discussion Scalar value is of @c BOOL type. Affected by @c -[RTKBBproPeripheral @c getAPTBrightnessInfoWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTBrightnessLevelofType:withLeftBrightness:andRightBrightness:Sync:withCompletion:] and @c -[RTKBBproPeripheral @c getAPTBrightnessStatusWithCompletionHandler:].
 */
@property (nonatomic, readonly, nullable) NSNumber *syncAPTBrightness;

/**
 * Return the current APT Volume level of this peripheral last cached.(versionNumber = 0x0104/0x0105)
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getAPTVolumeOutLevelWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTVolumeOutLevel:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *APTVolumeOutLevel;


#pragma mark - APT Power On Delay Time

/**
 * Return whether the APT NR is on of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Unit is second. Affected by @c -[RTKBBproPeripheral @c getAPTPowerOnDelayTimeWithCompletionHandler:], @c -[RTKBBproPeripheral @c setAPTPowerOnDelayTime:withCompletion:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *APTDelayTime;


#pragma mark - BudInfo

/**
 * Return the budType of this peripheral last cached. 0x00 Single, 0x01 RWS
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *budType;

/**
 * Return the connected bud of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *primaryBudSide;

/**
 * Return the channel corresponding to left and right of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *LRChannel;

/**
 * Return the left battery of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *singleOrLeftBattery;

/**
 * Return the right battery of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *rightBattery;

/**
 * Return the case battery of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Affected by @c -[RTKBBproPeripheral @c getBudInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *caseBattery;


#pragma mark - EQ2.0

/**
 * Return the number of the SPKEQ which could be saved in SOC.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *numberOfSavedSPKEQ;

/**
 * Return the number of the MICEQ which could be saved in SOC.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *numberOfSavedMICEQ;

/**
 * Return an index array representing the position of normal EQ in SPKEQSettings.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *normalEQIndexArray;

/**
 * Return an index array representing the position of gaming EQ in SPKEQSettings.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *gamingEQIndexArray;

/**
 * Return an index array representing the position of anc EQ in SPKEQSettings.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *ancEQIndexArray;

//@property (nonatomic, readonly, nullable) NSNumber *aptEQIndexes;

/**
 * Return a list of @c RTKBBproEQSetting objects which representing SPKEQ settings in remote device.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] . The returned @c RTKBBproEQSetting object may not be fully determine the parameter. Call @c -getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler: to make it full-fledged.
 */
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *SPKEQSettings;

/**
 * Return the current normal EQ index.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] and  @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentNormalEQIndex;

/**
 * Return the current gaming EQ index.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] and  @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentGamingEQIndex;

/**
 * Return the current anc EQ index.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] and  @c -[RTKBBproPeripheral @c setCurrentEQIndex:ofMode:withCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentANCEQIndex;

/**
 * Return the current sample rate.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentSampleRate;

/**
 * Return a list of the sample rates the peripheral supported.
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *supportedSampleRate;

/**
 * Return the current SPKEQ mode (normal, gaming or anc).
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentSPKEQMode;

/**
 * Return the current MICEQ mode (apt).
 *
 * @discussion Affected by @c -[RTKBBproPeripheral @c getEQInfoWithCompletionHandler:] .
 */
@property (nonatomic, readonly, nullable) NSNumber *currentMICEQMode;

@end


NS_ASSUME_NONNULL_END
