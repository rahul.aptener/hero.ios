//
//  RTKBBproPeripheral+ImageVersion.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/1/27.
//

#ifdef RTKHearingAidSDK
#import <RTKHearingAidSDK/RTKBBproPeripheral.h>
#import <RTKAudioConnectSDK/RTKBBproImage.h>
#elseif def ATAudioConnectSDK
#import <ATAudioConnectSDK/RTKBBproPeripheral.h>
#import <RTKAudioConnectSDK/RTKBBproImage.h>
#elseif def MLSAudioConnectSDK
#import <MLSAudioConnectSDK/RTKBBproPeripheral.h>
#import <RTKAudioConnectSDK/RTKBBproImage.h>
#else
#import <RTKAudioConnectSDK/RTKBBproPeripheral.h>
#import <RTKAudioConnectSDK/RTKBBproImage.h>
#endif

NS_ASSUME_NONNULL_BEGIN

/**
 * This category provides APIs for geting image version of a remote device.
 */
@interface RTKBBproPeripheral (ImageVersion)

/**
 * Start get image versions at specified bank of the device.
 *
 * @param isActiveBank A boolean value indicate if the images to get version is at active bank.
 * @param handler The completion handler to call when the load request is complete.
 */
- (void)getImageVersionsOfActiveBank:(BOOL)isActiveBank withCompletionHandler:(void(^)(BOOL success, NSError *err, NSArray <RTKBBproImage*> *bins))handler;

@end

NS_ASSUME_NONNULL_END
