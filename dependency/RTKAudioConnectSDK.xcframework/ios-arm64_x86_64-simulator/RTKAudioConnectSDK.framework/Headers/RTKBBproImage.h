//
//  RTKBBproImage.h
//  RTKOTASDK
//
//  Created by jerome_gu on 2019/4/16.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <Foundation/Foundation.h>

// This is synonymous with RTKOTASDK.RTKOTABin.
// Provided here to wrap image version info.

NS_ASSUME_NONNULL_BEGIN

/**
 * A general OTA bin.
 */
@interface RTKBBproImage : NSObject


/**
 * Version in integer.
 */
@property (readonly) uint32_t version;


/**
 * The binary name.
 */
@property (readonly) NSString *name;

/**
 * Human-readable version string.
 */
@property (readonly) NSString *versionString;


- (NSComparisonResult)compareVersionWith:(RTKBBproImage *)anotherBin;


@end


NS_ASSUME_NONNULL_END
