//
//  RTKAudioConnectSDK.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/1/23.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RTKBBproSDK.
FOUNDATION_EXPORT double RTKBBproSDKVersionNumber;

//! Project version string for RTKBBproSDK.
FOUNDATION_EXPORT const unsigned char RTKBBproSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RTKBBproSDK/PublicHeader.h>

#import <RTKAudioConnectSDK/RTKACServiceIdentifier.h>
#import <RTKAudioConnectSDK/RTKBBproType.h>

#import <RTKAudioConnectSDK/RTKBBproEQSetting.h>
#import <RTKAudioConnectSDK/RTKBBproEQSettingPlaceholder.h>

#import <RTKAudioConnectSDK/RTKANCSNotificationEvent.h>
#import <RTKAudioConnectSDK/RTKANCSNotificationAttribute.h>

#import <RTKAudioConnectSDK/RTKACRoutine.h>
#import <RTKAudioConnectSDK/RTKACBasicRoutine.h>
#import <RTKAudioConnectSDK/RTKACAudioRoutine.h>
#import <RTKAudioConnectSDK/RTKACEQRoutine.h>
#import <RTKAudioConnectSDK/RTKACMMIRoutine.h>
#import <RTKAudioConnectSDK/RTKACTTSRoutine.h>
#import <RTKAudioConnectSDK/RTKACRHARoutine.h>

#import <RTKAudioConnectSDK/RTKACConnectionUponGATT.h>
#import <RTKAudioConnectSDK/RTKACConnectionUponiAP.h>

#import <RTKAudioConnectSDK/RTKACConnectionManager.h>

#import <RTKAudioConnectSDK/RTKContactsAccess.h>

#import <RTKAudioConnectSDK/RTKBBproError.h>

#import <RTKAudioConnectSDK/RTKACBeaconMonitor.h>

#import <RTKAudioConnectSDK/RHAPayloadGenerator.h>

#import <RTKAudioConnectSDK/RTKHAUtility.h>

#import <RTKAudioConnectSDK/RTKACFilterInfo.h>
#import <RTKAudioConnectSDK/RTKACCapturedDataParser.h>


// Legacy APIs
// Only for compatibility, not recommend for new usage.
#import <RTKAudioConnectSDK/RTKBBproProfile.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheral.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheral+Deprecated.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheral+LG.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheral+CustomeReadWrite.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheral+ImageVersion.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheralStateCache.h>

#import <RTKAudioConnectSDK/RTKBBproProfile+Dump.h>
#import <RTKAudioConnectSDK/RTKBBproDumpPeripheral.h>
