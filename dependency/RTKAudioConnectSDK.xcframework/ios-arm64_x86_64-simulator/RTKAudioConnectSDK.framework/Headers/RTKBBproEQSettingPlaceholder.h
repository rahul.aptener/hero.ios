//
//  RTKBBproEQSettingPlaceholder.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/7/8.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <RTKAudioConnectSDK/RTKBBproEQSetting.h>

NS_ASSUME_NONNULL_BEGIN

/// A object that provides more infomation related to load state.
@interface RTKBBproEQSettingPlaceholder : RTKBBproEQSetting

@property (nonatomic, readonly) NSUInteger index;

- (instancetype)initWithIndex:(NSUInteger)idx;

/// Set a new human-readable name of this object.
@property (nullable) NSString *name;

// Indicate if the Setting is synced to remote peripheral.
@property (nonatomic, readonly) BOOL determined DEPRECATED_MSG_ATTRIBUTE("Use -resolved instead.");

/// Return a boolean value that indicate whether parameters is updated by device data.
@property (nonatomic, readonly) BOOL resolved;

/// Return a boolean value indicate whether parameters did modified after the initial retrieve.
@property (nonatomic, readonly) BOOL modified;

- (void)setInitialParameterData:(NSData *)parameterData DEPRECATED_MSG_ATTRIBUTE("Use -setInitialParameterData:ofSampleRate: instead.");

/// Parse the parameterData and update this instance's setting parameters.
- (void)setInitialParameterData:(NSData *)parameterData ofSampleRate:(RTKBBproSampleRate)sampleRate;

/// Return the sample rate of which when parse the parameter data.
///
/// - Returns `RTKBBproSampleRate_unknown` if this object has not parse a paramter data.
@property (nonatomic, readonly) RTKBBproSampleRate realizationSampleRate;


@property (nonatomic) BOOL canBeSaved;

/// Whether the compensation based on hearing loss has applied
@property BOOL hearingLossCompensationApplied;

@end

NS_ASSUME_NONNULL_END
