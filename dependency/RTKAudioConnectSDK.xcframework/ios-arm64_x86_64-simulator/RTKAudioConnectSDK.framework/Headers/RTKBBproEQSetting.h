//
//  RTKBBproEQSetting.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/2/25.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef float RTKEQFrequency;
typedef float RTKEQGain;
typedef float RTKEQQFactor;

/// The possible Sample Rate related to audio signal
typedef NS_ENUM(NSUInteger, RTKBBproSampleRate) {
    RTKBBproSampleRate_8K,  ///< 8k hz
    RTKBBproSampleRate_16K, ///< 16k hz
    RTKBBproSampleRate_32K, ///< 32k hz
    RTKBBproSampleRate_44K1,    ///< 44.1k hz
    RTKBBproSampleRate_48K,     ///< 48k hz
    RTKBBproSampleRate_88K2,    ///< 88.2k hz
    RTKBBproSampleRate_96K,     ///< 96k hz
    RTKBBproSampleRate_192K,    ///< 192k hz
    RTKBBproSampleRate_unknown = 0xFF, ///< sample rate is not known
};


/// The possbile Filter type for EQ effect
typedef NS_ENUM(NSUInteger, RTKBBproEQFilter) {
    RTKBBproEQFilter_PEAK,
    RTKBBproEQFilter_ShelvingLP,
    RTKBBproEQFilter_ShelvingHP,
    RTKBBproEQFilter_LowPass,
    RTKBBproEQFilter_HighPass,
    RTKBBproEQFilter_PEAKCOOKBOOK,
    RTKBBproEQFilter_BandPass,
    RTKBBproEQFilter_BandReject,
    RTKBBproEQFilter_AllPass,
    RTKBBproEQFilter_AllEQ,
};


/// An `RTKBBproEQSetting` object describe parameters of a EQ entry used or to be used on a remote RTKBBproPeripheral device.
///
/// An `RTKBBproSetting` is created with zero parameters, which means that all attribute (i.e globalGain, stage frequency gains,stage Q) value is 0. To represet eq settings of a device, the eq para data should be retrieved and pass to -updateSettingWithParameterData:ofSampleRate:. When set eq effect of a remote device, send data returned by -parameterDataOfSampleRate: to a device.
@interface RTKBBproEQSetting : NSObject <NSSecureCoding, NSCopying> {
    @protected
    NSString *_name;
}

/// Create and return a new `RTKBBproEQSetting` object which is preset with Flat effect.
+ (instancetype)FlatEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Acoustic effect.
+ (instancetype)AcousticEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Bass Booster effect.
+ (instancetype)BassBoosterEQSetting;

///* Create and return a new `RTKBBproEQSetting` object which is preset with Bass Reducer effect.
+ (instancetype)BassReducerEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Classical effect.
+ (instancetype)ClassicalEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Hip Hop effect.
+ (instancetype)HipHopEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Jazz effect.
+ (instancetype)JazzEQSetting;

/// Create and return a new `RTKBBproEQSetting` object which is preset with Rock effect.
+ (instancetype)RockEQSetting;

// MARK: -

/// Return a human-readable name of this `RTKBBproEQSetting` object.
@property (readonly, nullable) NSString *name;
//@property (readonly) NSString *localizedName;

/// Return an `RTKBBproEQSetting` instance with the specified name.
///
/// The returned `RTKBBproEQSetting` instance have a stageCount equal to 10.  Frequency list is set to a default list value, all stage filter is peak, and all Qs are set to 2 and all gains are set to 0.
- (instancetype)initWithName:(nullable NSString *)name;

/// Return an `RTKBBproEQSetting` instance with the specified name and stage gain list.
///
/// The returned `RTKBBproEQSetting` instance have a stageCount set to item count of passed in gainNumbers.  Frequency list is set to a default list value, all stage filter is peak, and all Qs are set to 2, each stage gain is set to item value of gainNumbers list.
- (instancetype)initWithName:(NSString *)name gainNumbers:(NSArray <NSNumber*> *)gainNumbers;


/// Return this object's stage count or set to a new value.
///
/// When set, the new value should not large than 10. Setting a new value will reset stage frequency list, gain list, Q list to default value.
@property (nonatomic) NSUInteger stageCount;


/// Return this object's global gain value or set to a new value.
@property (nonatomic) double globalGain;


/// Return frequency value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to return frequency. Should not exceed stageCount.
- (RTKEQFrequency)frequencyAtIndex:(NSUInteger)idx;


/// Set a new frequency value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to set frequency. Should not exceed stageCount.
- (void)setFrequency:(RTKEQFrequency)freq atIndex:(NSUInteger)idx;


/// Return gain value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to return gain. Should not exceed stageCount.
- (RTKEQGain)gainAtIndex:(NSUInteger)idx;


/// Set a new gain value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to set gain. Should not exceed stageCount.
- (void)setGain:(RTKEQGain)gain atIndex:(NSUInteger)idx;


/// Return Q value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to return Q. Should not exceed stageCount.
- (RTKEQQFactor)qAtIndex:(NSUInteger)idx;


/// Set a new Q value of a stage specified by index.
///
/// - Parameter idx: The index of the stage to set Q. Should not exceed stageCount.
- (void)setQ:(RTKEQQFactor)q atIndex:(NSUInteger)idx;


/// Return Filter type of a stage specified by index.
///
/// - Parameter idx: The index of the stage to return type. Should not exceed stageCount.
- (RTKBBproEQFilter)filterAtIndex:(NSUInteger)idx;


/// Set Filter type of a stage specified by index.
///
/// - Parameter idx: The index of the stage to set type. Should not exceed stageCount.
- (void)setFilter:(RTKBBproEQFilter)filter atIndex:(NSUInteger)idx;


/// Reset all setting parameters to default value.
///
/// `StageCount` is reset to 10, `globalGain` is reset to 0, `frequency` list is reset to default list, all gains is reset to 0, Qs are reset to 2, and all stage use PEAK filter.
- (void)reset;

// MARK: -

/// Calculate and return the raw parameter data used for send to config remote device.
///
/// - Parameter sampleRate: The sample rate under which calculate data.
- (NSData *)parameterDataOfSampleRate:(RTKBBproSampleRate)sampleRate;

// synonymous with -serializedDataAt44P1KFrequency
- (NSData *)serializedData DEPRECATED_MSG_ATTRIBUTE("Use parameterDataOfSampleRate instead.");

- (NSData *)serializedDataAt44P1KFrequency  DEPRECATED_MSG_ATTRIBUTE("Use parameterDataOfSampleRate instead.");
- (NSData *)serializedDataAt48KFrequency    DEPRECATED_MSG_ATTRIBUTE("Use parameterDataOfSampleRate instead.");
- (NSData *)serializedDataAt96KFrequency DEPRECATED_MSG_ATTRIBUTE("Use parameterDataOfSampleRate instead.");


/// Calculate and return the extended raw parameter data used for send to config remote device.
///
/// - Parameter sampleRate: Sample rate which the calculated parameter used for.
- (NSData *)parameterDataWithExtraInfoOfSampleRate:(RTKBBproSampleRate)sampleRate;


/// Calculate and return the extended raw parameter data used for send to config remote device.
///
/// - Parameter sampleRate: Sample rate which the calculated parameter used for.
- (NSData *)parameterDataWithNewExtraInfoDataAttachedOfSampleRate:(RTKBBproSampleRate)sampleRate;

// MARK: - 

- (void)updateSettingWithParameterData:(NSData *)parameterData DEPRECATED_MSG_ATTRIBUTE("Use -updateSettingWithParameterData:ofSampleRate instead.");

- (void)updateSettingWithParameterExtraInfoData:(NSData *)data DEPRECATED_MSG_ATTRIBUTE("Use -updateSettingWithParameterExtraInfoData:ofSampleRate instead.");
- (void)updateSettingWithNewParameterExtraInfoData:(NSData *)data DEPRECATED_MSG_ATTRIBUTE("Use -updateSettingWithNewParameterExtraInfoData:ofSampleRate instead.");


/// Parse the parameterData and update this instance's setting parameters.
///
/// - Parameter parameterData: The raw para data received from remote device.
/// - Parameter sampleRate: The sample rate of the parameterData.
- (void)updateSettingWithParameterData:(NSData *)parameterData ofSampleRate:(RTKBBproSampleRate)sampleRate;


/// Parse the parameterData and update this instance's setting parameters.
///
/// - Parameter data: The raw para data received from remote device.
/// - Parameter sampleRate: The sample rate of the parameterData.
- (void)updateSettingWithParameterExtraInfoData:(NSData *)data ofSampleRate:(RTKBBproSampleRate)sampleRate;


/// Parse the parameterData and update this instance's setting parameters.
///
/// - Parameter data: The raw para data received from remote device.
/// - Parameter sampleRate: The sample rate of the parameterData.
- (void)updateSettingWithNewParameterExtraInfoData:(NSData *)data ofSampleRate:(RTKBBproSampleRate)sampleRate;

@end


NS_ASSUME_NONNULL_END
