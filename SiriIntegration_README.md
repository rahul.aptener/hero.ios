# SiriKit Shortcuts

This guide outlines how to integrate SiriKit intents to control various actions, such as starting, stopping, finding, and opening the seat of a bike using Siri. Below are the steps to implement the necessary intents, handlers, and UI components for a SwiftUI application.

## 1. Create Intents in Intent Definition File

### Step 1: Add a SiriKit Intent Definition File
1. In Xcode, add a new file and choose **SiriKit Intent Definition File**.
2. Name the file, for example, `ShortcutIntents`.

### Step 2: Define Intents
1. Open the `ShortcutIntents.intentdefinition` file.
2. Add new intents by clicking "+" to define `StartBike`, `StopBike`, `FindBike`, and `OpenSeat` intents.

### Step 3: Configure Intent Properties
- Set the title and description for each intent (e.g., **Start the Bike**, **Stop the Bike**).
- Set the response file to return the result (e.g., result as a property) and add a voice dialog.

#### Example of Siri Voice Dialog:
- "Bike started `result`" or "Bike stopped `result`."

#### Step 4: Create a new target for Intent Extensions
- In Xcode, add a new target and choose **Intents Extension**.
- This also creates a **Intents UI Extension** file.
- In Intents Extension target, there will be **IntentHandler**
- In Intents UI Extension target, there will be **IntentViewController**

---

## 2. Correcting the Intent Handlers

Ensure all intent handlers correctly implement the success methods and post necessary notifications.

### Intent Handlers Example:

```swift

public class IntentHandler: INExtension {
    public override func handler(for intent: INIntent) -> Any? {
        switch intent {
        case is StartBikeIntent:
            return StartBikeIntentHandler()
        case is StopBikeIntent:
            return StopBikeIntentHandler()
        case is FindBikeIntent:
            return FindBikeIntentHandler()
        case is OpenSeatIntent:
            return OpenSeatIntentHandler()
        default:
            fatalError("Unhandled Intent error: \(intent)")
        }
    }
}

public class StartBikeIntentHandler: NSObject, StartBikeIntentHandling {
    public func handle(intent: StartBikeIntent, completion: @escaping (StartBikeIntentResponse) -> Void) {
      
        completion(StartBikeIntentResponse.success(result: "Successfully"))

        UserDefaults.standard.set(true, forKey: "showRedView")
    }
}

extension Notification.Name {
    public static let didCompleteStartBikeIntent = Notification.Name("didCompleteStartBikeIntent")
}

public class StopBikeIntentHandler: NSObject, StopBikeIntentHandling {
    public func handle(intent: StopBikeIntent, completion: @escaping (StopBikeIntentResponse) -> Void) {

        completion(StopBikeIntentResponse.success(result: "Bike stopped successfully"))

        UserDefaults.standard.set(true, forKey: "showStopBikeView")
        NotificationCenter.default.post(name: .didCompleteStopBikeIntent, object: nil)
    }
}


extension Notification.Name {
    public static let didCompleteStopBikeIntent = Notification.Name("didCompleteStopBikeIntent")
}

public class FindBikeIntentHandler: NSObject, FindBikeIntentHandling {
    public func handle(intent: FindBikeIntent, completion: @escaping (FindBikeIntentResponse) -> Void) {
        
        completion(FindBikeIntentResponse.success(result: "Bike found successfully"))

        UserDefaults.standard.set(true, forKey: "showFindBikeView")
        NotificationCenter.default.post(name: .didCompleteFindBikeIntent, object: nil)
    }
}

extension Notification.Name {
    public static let didCompleteFindBikeIntent = Notification.Name("didCompleteFindBikeIntent")
}
public class OpenSeatIntentHandler: NSObject, OpenSeatIntentHandling {
    public func handle(intent: OpenSeatIntent, completion: @escaping (OpenSeatIntentResponse) -> Void) {
        
        completion(OpenSeatIntentResponse.success(result: "Seat Opened successfully"))

        UserDefaults.standard.set(true, forKey: "showFindBikeView")
        NotificationCenter.default.post(name: .didCompleteOpenSeatIntent, object: nil)
    }
}

extension Notification.Name {
    public static let didCompleteOpenSeatIntent = Notification.Name("didCompleteOpenSeatIntent")
}

```

### Link Notifications and Handlers 
Ensure the correct notifications are being posted from each intent handler (as shown above).

Add Notification Handler:
```swift
import Combine

public class NotificationHandler: ObservableObject {
    @Published public var activeView: ActiveView? = nil
    
    public init() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleStartBikeIntent), name: .didCompleteStartBikeIntent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleStopBikeIntent), name: .didCompleteStopBikeIntent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFindBikeIntent), name: .didCompleteFindBikeIntent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOpenSeatIntent), name: .didCompleteOpenSeatIntent, object: nil)
    }

    @objc public func handleStartBikeIntent() {
        DispatchQueue.main.async {
            self.activeView = .startBike
        }
    }

    @objc public func handleStopBikeIntent() {
        DispatchQueue.main.async {
            self.activeView = .stopBike
        }
    }

    @objc public func handleFindBikeIntent() {
        DispatchQueue.main.async {
            self.activeView = .findBike
        }
    }
    @objc public func handleOpenSeatIntent() {
        DispatchQueue.main.async {
            self.activeView = .openseat
        }
    }
}

public enum ActiveView: Identifiable {
    case startBike, stopBike, findBike, openseat
    
    public var id: String {
        switch self {
        case .startBike:
            return "startBike"
        case .stopBike:
            return "stopBike"
        case .findBike:
            return "findBike"
        case .openseat:
            return "openseat"
        }
    }
}

```

## 3. Setting Up Shortcuts in ContentView

### Step 1: Triggering Siri Buttons

Ensure the buttons in ContentView work with the shortcuts you've defined. For example, when the user adds the "Start Bike" shortcut:

Create SiriButton:

```swift
import SwiftUI
import IntentsUI

public struct SiriButton: UIViewControllerRepresentable {
    public let shortcut: INShortcut?
    
    public init(shortcut: INShortcut?) {
        self.shortcut = shortcut
    }
    
    public func makeUIViewController(context: Context) -> SiriUIViewController {
        return SiriUIViewController(shortcut: shortcut)
    }
    
    public func updateUIViewController(_ uiViewController: SiriUIViewController, context: Context) {
    }
}

public class SiriUIViewController: UIViewController {
    public var shortcut: INShortcut?
    
    public init(shortcut: INShortcut?) {
        self.shortcut = shortcut
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = INUIAddVoiceShortcutButton(style: .blackOutline)
        button.shortcut = shortcut
        
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])

        button.delegate = self
    }
}

extension SiriUIViewController: INUIAddVoiceShortcutButtonDelegate {
    public func present(_ addVoiceShortcutViewController: INUIAddVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        addVoiceShortcutViewController.delegate = self
        addVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(addVoiceShortcutViewController, animated: true)
    }
    
    public func present(_ editVoiceShortcutViewController: INUIEditVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        editVoiceShortcutViewController.delegate = self
        editVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(editVoiceShortcutViewController, animated: true)
    }
}

extension SiriUIViewController: INUIAddVoiceShortcutViewControllerDelegate {
    public func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true)
    }

    public func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        controller.dismiss(animated: true)
    }
}

extension SiriUIViewController: INUIEditVoiceShortcutViewControllerDelegate {
    public func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didUpdate voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true)
    }

    public func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didDeleteVoiceShortcutWithIdentifier deletedVoiceShortcutIdentifier: UUID) {
        controller.dismiss(animated: true)
    }

    public func editVoiceShortcutViewControllerDidCancel(_ controller: INUIEditVoiceShortcutViewController) {
        controller.dismiss(animated: true)
    }
}

```

Example usage for SiriButton:

```swift
Button(action: {
    showStartBikeShortcutView = true
}) {
    HStack {
        Image(systemName: "bicycle")
            .resizable()
            .frame(width: 24, height: 24)
            .foregroundColor(.white)
        
        Text("Add Start Bike Shortcut")
            .foregroundColor(.white)
    }
    .padding()
    .background(Color.blue)
    .cornerRadius(8)
}
.padding()

if showStartBikeShortcutView {
    SiriButton(shortcut: createStartBikeShortcut())
        .frame(width: 200, height: 50)
}
```
### Step 2: Siri Shortcut Creation Methods

Make sure you have a method that creates shortcuts for each intent:

```swift
public func createStartBikeShortcut() -> INShortcut? {
    let intent = StartBikeIntent()
    intent.suggestedInvocationPhrase = "Start the Bike"
    return INShortcut(intent: intent)
}

public func createStopBikeShortcut() -> INShortcut? {
    let intent = StopBikeIntent()
    intent.suggestedInvocationPhrase = "Stop the Bike"
    return INShortcut(intent: intent)
}

public func createFindBikeShortcut() -> INShortcut? {
    let intent = FindBikeIntent()
    intent.suggestedInvocationPhrase = "Find My Bike"
    return INShortcut(intent: intent)
}

public func createOpenSeatShortcut() -> INShortcut? {
    let intent = OpenSeatIntent()
    intent.suggestedInvocationPhrase = "Open the Seat"
    return INShortcut(intent: intent)
}
```
## 4. Final Steps for Intents in Info.plist
Ensure that your app's Info.plist includes the SiriKit capability with the required intents declared.

### Sample UI Images

![Example Image](doc_assets/ref1.png)
![Example Image](doc_assets/ref2.png)
![Example Image](doc_assets/ref2.png)

