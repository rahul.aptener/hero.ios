# BluConnect SDK

## Introduction

BluConnect is a SDK designed to help developers interact with BluArmor Devices using Bluetooth Low Energy technology. It simplifies the process of discovering, connecting, and communicating with these devices.

Key features of BluConnect include automatic device connection, device discovery, and the ability to check the status of the device's Bluetooth and location services. It also provides access to a list of previously connected devices and allows users to customize their preferences.

To use BluConnect in your iOS project, you need to include the necessary library files and configure your project accordingly. Additionally, the SDK requires certain permissions, such as bluetooth, access to location, contacts, and background data processing to function properly.

## Table of Contents

- [Getting Started](#getting-started)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [API Reference](#api-reference)

## Getting Started

Guide on how to get started with the SDK, including:
### Prerequisites

Before you begin integrating BluConnect into your iOS project, ensure that you have the following prerequisites installed and configured:

#### Dependencies

- **XCode**: Ensure your project is configured to use SPM (Swift Package Manager) for dependency management.

- **XCode Version**: 15.x
- **Swift Version**: 5.6+

#### iOS SDK Compatibility

- **Minimum iOS Deployment**: 14

- **Tested upto iOS version**: 17.4.1

#### SDK Integration

- **SDK XCFramework File**: Add the `BluConnect.xcframework` provided by BluConnect.

#### Permissions

- **Permissions**: Identify any permissions required by opted feature and ensure they are declared in the MainTarget's info.plist file of your project.

- Core SDK permission
```xml
   Privacy - Bluetooth Peripheral Usage 
   Privacy - Bluetooth Always Usage
   Required background modes
        - App registers for location updates
        - App communicates using CoreBluetooth
        - App shares data using CoreBluetooth
        - App downloads content from the network
        - App processes data in the background
```

- SDK BLE permission
```xml
   Privacy - Bluetooth Peripheral Usage
```

- Voice note recording permission
```xml
    Privacy - Microphone Usage
    Privacy - Location Usage
    Privacy - Location Always Usage
    Privacy - Location Always and When In Use Usage 
```

- Dialer & Speed dial permission
```xml
    Privacy - Contacts Usage
```

- Over the air device firmware update
```xml
    App downloads content from the network
```



#### Initialization

- **Initialization Code**: Extend `BluConnectService()` in the base app. Viewmodel recommended.

#### Configuration

- **Configuration Settings**: Configure and provide at least [Core SDK permission](#core-sdk-permission) and [SDK BLE permission](#sdk-ble-permission) for proper functioning of the BluConnect SDK within your project.

## Installation

Detailed instructions on how to install the SDK:
- : Add `BluConnect.xcframework` to the iOS target

- Thirdparty dependencies:
```groovy
    https://github.com/Swinject/Swinject
    https://github.com/apple/swift-protobuf.git
    https://github.com/NordicSemiconductor/lOS-DFU-Library
    https://github.com/firebase/firebase-ios-sdk
```

## Configuration

Information on how to configure the SDK for different use cases, including:
- **Initialization**: Start by creating a new instance of BluConnectService. Instance must we kept active  the app session.
```swift
class BaseAppViewModel:BluConnectService{
    override func onConnectionStateChange(state: ConnectionState) {
        ...
    }
}
```
- If a BluArmor device is discovered within Bluetooth range (approximately 10 meters), the connection procedure will commence automatically. As the connection establishes, any changes in `ConnectionState` will be promptly communicated through the onConnectionStateChange(...) method call. Following are the possible connection states.
```swift
   /**
 The **ConnectionState** represents the state of SDK. To access the state extend the **BluConnectManager** class and inherit the **BluConnectManagerDelegate**
 */
public enum ConnectionState:Equatable {
    
    /// SDK is not ready to scan, look for the *reason* - **NotReadyReason** for more info
    case ScannerNotReady(reason: NotReadyReason)
    
    /// Connection request initiated
    case Connecting(bluArmorDevice: AdvertisingBluArmorDevice)
    
    /// Device is connected and discovering Services and Characteristics
    case Connected(bluArmorDevice: AdvertisingBluArmorDevice?)
    
    /// Connection attempt failed
    case Failed(bluArmorDevice: AdvertisingBluArmorDevice?)
    
    /// Device is ready to operate
    case DeviceReady(bluArmorDevice: ConnectedBluArmorDevice)
    
    /// Device is disconnecting
    case Disconnecting(bluArmorDevice: AdvertisingBluArmorDevice?)
    
    /// Discovering BluArmor range of devices
    case Discovery(bluArmorDevices: [AdvertisingBluArmorDevice])
}

public enum NotReadyReason : Int {
    /// Represents deafault state, SDK will recover on its own
    case Unknown = 0
    
    /// Bluetooth manager is restarting
    case Resetting = 1
    
    /// Bluetooth is not supported
    case Unsupported = 2
    
    /// Require bluetooth access permission
    case Unauthorized = 3
    
    /// Phone's bluetooth is turned Off
    case PoweredOff = 4
    
    /// Phone's bluetooth is turned On
    case PoweredOn = 5
}

```

## Usage

- Identify when device is paired: The `onConnectionStateChange(...)` event emits the ConnectionState.Ready state, it indicates that the device is successfully connected and ready for operation.
- To obtain the connected BluArmor device instance, utilize either `ConnectionState.DeviceReady.device` or the `getActiveDevice()` method from `BluConnectService`.
```swift
    switch connectionState {
        case .DeviceReady(let bluArmorDevice):
            if let hS1Device = bluArmorDevice as? HS1{
                // Update the UI
            }
            break
        ...
    }
```

## API Reference

Detailed documentation of all classes, methods, and attributes provided by the SDK, organized into sections such as:

#### BluConnect SDK provides access to various features and controls of BluArmor HS1.
 * [DeviceInfoHS1]() Version metadata
 * [Battery]() Battery updates
 * [Volume]() Offers volume controls
 * [MediaPlayer]() Offers media player controls
 * [Dialer]() Offers phone call controls
 * [VoiceAnnouncement]() Play custom text announcements and media files. **Support for HS1 is yet to be enabled**
 * [VoiceNoteRecorder]() Records voice note with location data. **Support for HS1 is yet to be enabled**
 * [VoiceAssistant]() Invoke voice assistant
 * [ButtonClickHS1]() Observe button clicks
 * [DialerSpeedDial]() Speed dial intercom
 * [OtaSwitch]() Start OTA update

### Battery
Once the device is paired, the battery level updates with each alteration in level and whenever the charger is plugged in or disconnected.

- **Emits State**: `BatteryLevel(Int, ChargingState)`
```swift
    enum  ChargingState {
        case UNKNOWN, CHARGING, FULL, DRAINING
    }
```
- **To observe** [BatteryLevel]()
```swift
    func batteryObserver(batteryLevel:BatteryLevel){
        //Update UI
    }

    hs1.battery.observe(onUpdate:self.batteryObserver(batteryLevel:))
```

### Dialer
Dialer helps manage phone calls on a device. It provides functionality to interact with incoming and outgoing calls, such as answering, dropping, and rejecting calls.

- **Emits State**: `DialerState`
    - Unknown: Represents an undetermined state of the phone call, default if state cannot be determined.

    - Idle: Represents no ongoing call activity.

    - Missed: Represents a missed incoming call.
    
    - Incoming: Data class containing info about incoming call (phone number, name).

    - Active: Data class containing info about ongoing active call (phone number, name).

    - Outgoing: Data class containing info about outgoing call (phone number, name).
<br>*_phone number and name if available of the caller_

- **To observe** [DialerState]()
```swift

    func dialerStateObserver(dialerState:DialerState){
        //Update UI
    }

    hs1.dialer.observe(onUpdate:self.dialerStateObserver(dialerState:))

```

- **Functions to manage phone calls**
    * acceptPhoneCall(): This function answers an incoming phone call.

    * endPhoneCall(): This function terminates an ongoing phone call.

    * rejectPhoneCall(): This function declines or rejects an incoming phone call.

### MediaPlayer
MediaPlayer defines an interface for controlling media playback. It includes methods for playing, pausing, moving to the next and previous tracks, and observing changes in the media player's state

- **Emits State**: `MediaPlayerState`

    - Idle: This state indicates that no media is currently playing or paused. It signifies a state of inactivity or standby where the media player is not engaged in playback.

    - Playing: This state signifies that media is actively playing. It includes metadata about the currently playing track and artist. The metadata parameters `track` and `artist` provide information about the currently active music being played.

- **To observe** [MediaPlayerState]()
```swift
    private val mediaPlayerStateObserver = PropertyObserver<MediaPlayer.MediaPlayerState>{ state ->
        //Update UI
    }

    hs1.mediaPlayer.observe(mediaPlayerStateObserver)
```

- **Functions to control media playback**

    - playMusic(): Plays music when the device is idle.

    - pauseMusic(): Pauses the music if music is currently playing.

    - nextMusic(): Changes the music to the next track.

    - previousMusic(): Changes the music to the previous track.

### Volume
Volume defines an interface which handles volume control for a device. It emits information about the volume level. It provides methods to observe changes in the volume level and to update various aspects of volume control


- **Emits State**: `VolumeLevel`

    - volumeLevel: Integer ranges from 0 to 15.

    - dynamicMode: Boolean state signifies that volume will be controlled with respect to riding speed.

- **To observe** [VolumeLevel]()
```swift

    func volumeLevelObserver(volumeLevel:VolumeLevel){
        //Update UI
    }

    hs1.volume.observe(onUpdate:self.volumeLevelObserver(volumeLevel:))
```

- **Functions to control volume level**

    - changeVolume(level: Int): Updates the volume level to the desired value within the range of 0 to 15.

    - enableDynamicMode()

    - disableDynamicMode()


### In addition to above feture sets there are system level control to `power OFF`, `reboot`, `rename` and `factory reset` the HS1
```swift 
    /**
     * Turn off the device.
     * This function turns off the Smart Helmet Headset HS1 device.
     */
    func turnOff()

    /**
     * Restart the device.
     * This function restarts the Smart Helmet Headset HS1 device.
     */
    func reboot()

    /**
     * Change the device name.
     *
     * @param name The new device name. "HS1_" will be auto appended before the name.
     * This function changes the name of the Smart Helmet Headset HS1 device.
     */
    func rename(name: String)

    /**
     * Perform a factory reset, wiping all data from the device.
     * This function resets the Smart Helmet Headset HS1 device to its factory settings,
     * erasing all stored data.
     */
    func factoryReset()
```


_Additionally, using Doc references will be advantageous for implementation guidance._